from django.urls import path, re_path
from . import views

app_name = 'django_practice'
urlpatterns = [
	path('', views.index,name= 'index'),
	path('register',views.register, name='register'),
	path('change_password',views.change_password, name="change_password"),
	path('login', views.login_view, name="login"),
	path('logout', views.logout_view, name="logout"),
	path('add_item', views.add_item, name="add_item"),
	path('add_event', views.add_event, name="add_event"),
	path('update_profile', views.update_profile, name="update_profile"),
	path('grocery<int:groceryitem_id>/', views.groceryitem, name = 'viewgroceryitem'),
	path('grocery<int:groceryitem_id>/edit', views.update_item, name='update_item'),
	path('grocery<int:groceryitem_id>/delete', views.delete_item, name='delete_item'),
	path('event<int:eventitem_id>', views.eventitem, name = 'vieweventitem'),
	path('event<int:eventitem_id>/edit', views.update_event, name='update_event'),
	path('event<int:eventitem_id>/delete', views.delete_event, name='delete_event'),


	
]

